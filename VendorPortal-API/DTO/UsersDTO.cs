﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VendorPortal_API.Models.Users;

namespace VendorPortal_API.DTO
{
    public class UsersDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string VendorID { get; set; }
    }
}
