﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VendorPortal_API.Models.BRdata;

namespace VendorPortal_API.DTO
{
    public class ItemDTO
    {
        public string UPC { get; set; }
        public string Desc { get; set; }
        public short? Dept { get; set; }
        public string Size { get; set; }
        public string Code { get; set; }
        public short? Pack { get; set; }
        public string Vendor { get; set; }
        public short? Qty { get; set; }
        public decimal? Retail { get; set; }
        public decimal? Cost { get; set; }
    }
}
