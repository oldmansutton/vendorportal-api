﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VendorPortal_API.Models.BRdata;

namespace VendorPortal_API.DTO
{
    public class ItemHistoryDTO
    {
        public string UPC { get; set; }
        public string Store { get; set; }
        public short QuantitySold { get; set; }
        public decimal AverageRetail { get; set; }
        public decimal AverageGM { get; set; }
    }
}
