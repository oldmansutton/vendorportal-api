﻿namespace VendorPortal_API.DTO
{
    public class SubsPC_DTO
    {
        public string VendorId { get; set; }
        public int SubId { get; set; }
        public string Upc { get; set; }
        public string Description { get; set; }
        public short OCasePack { get; set; }
        public double OCaseCost { get; set; }
        public short OQty { get; set; }
        public double ORetail { get; set; }
        public short NCasePack { get; set; }
        public double NCaseCost { get; set; }
        public short NQty { get; set; }
        public double NRetail { get; set; }
    }
}
