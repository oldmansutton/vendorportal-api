﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using VendorPortal_API.Models.Submissions;
using VendorPortal_API.Models.BRdata;
using VendorPortal_API.DTO;

namespace VendorPortal_API.Services
{
    public interface ISubmissionService
    {
        string brUPC(string inUPC);
        string GetSubVendor(int subID);
        IEnumerable<Submissions> GetAll();
        IEnumerable<Submissions> GetAllPending(string vID, string rID = "none");
        IEnumerable<Submissions> GetAllApproved(string vID, string rID = "none");
        IEnumerable<Submissions> GetAllType(int subTypeID);
        IEnumerable<SubmissionNewItem> GetSNIDetails(int subID, string vendor);
        IEnumerable<SubsPC_DTO> GetCPDetails(int subID, string vendor);
        IEnumerable<SubmissionTpr> GetTPRDetails(int subID, string vendor);
        IEnumerable<ItemHistoryDTO> GetItemMovement(string upc, DateTime sDt, DateTime eDt);
    }

    public class SubmissionService : ISubmissionService
    {
        private VendorPortalContext _vendorPortalContext;
        private BRdataContext _bRdataContext;

        public SubmissionService(VendorPortalContext vendorPortalContext, BRdataContext bRdataContext)
        {
            _vendorPortalContext = vendorPortalContext;
            _bRdataContext = bRdataContext;
        }

        public string brUPC(string inUPC)
        {
            return inUPC.Trim().PadLeft(15, '0');
        }

        public string GetSubVendor(int subID)
        {
            return _vendorPortalContext.Submissions.Where(s => s.SubId == subID).Select(s => s.VendorId).SingleOrDefault();
        }

        public IEnumerable<Submissions> GetAll()
        {
            return _vendorPortalContext.Submissions;
        }

        public IEnumerable<Submissions> GetAllPending(string vID, string rID = "none")
        {
            if (rID == "admin")
            {
                return _vendorPortalContext.Submissions.Where(s => s.SubApproved == false);
            }
            return _vendorPortalContext.Submissions.Where(s => s.SubApproved == false && s.VendorId == vID);
        }

        public IEnumerable<Submissions> GetAllApproved(string vID, string rID = "none")
        {
            if (rID == "admin")
            {
                return _vendorPortalContext.Submissions.Where(s => s.SubApproved == true);
            }
            return _vendorPortalContext.Submissions.Where(s => s.SubApproved == true && s.VendorId == vID);
        }

        public IEnumerable<Submissions> GetAllType(int subTypeID)
        {
            return _vendorPortalContext.Submissions.Where(s => s.SubTypeId == subTypeID);
        }

        public IEnumerable<SubmissionNewItem> GetSNIDetails(int subID, string vendor)
        {
            return _vendorPortalContext.SubmissionNewItem.Where(s => s.SubId == subID && s.VendorId == vendor);
        }

        public IEnumerable<SubsPC_DTO> GetCPDetails(int subID, string vendor)
        {
            List<SubsPC_DTO> SubsPC = new List<SubsPC_DTO>();
            IEnumerable<SubmissionCostPrice> tmpDeets = _vendorPortalContext.SubmissionCostPrice.Where(s => s.SubId == subID && s.VendorId == vendor);
            foreach (SubmissionCostPrice x in tmpDeets)
            {
                SubsPC_DTO tmpS = new SubsPC_DTO();
                var _tmpUPC = brUPC(x.Upc);
                var _cost = _bRdataContext.VendorCost.Where(i => i.Upc == _tmpUPC && i.Vendor == x.VendorId).Select(i => new { i.Pack, i.CaseCost }).SingleOrDefault();
                if (_cost == null)
                {
                    return null;
                }
                var _price = _bRdataContext.Price.Where(i => i.Upc == _tmpUPC && i.Store == "00000A").Select(i => new { i.BasePrcQty, i.BasePrice }).SingleOrDefault();
                if (_price == null)
                {
                    return null;
                }
                var _tmpDesc = _bRdataContext.Item.Where(i => i.Upc == _tmpUPC).Select(i => new { i.Description }).SingleOrDefault();
                if (_tmpDesc == null)
                {
                    return null;
                }
                tmpS.VendorId = x.VendorId;
                tmpS.SubId = x.SubId;
                tmpS.Upc = x.Upc;
                tmpS.NCaseCost = x.ItemNewCaseCost;
                tmpS.NCasePack = x.ItemNewCasePack;
                tmpS.NQty = x.ItemNewSrpQty;
                tmpS.NRetail = x.ItemNewSrp;
                tmpS.Description = _tmpDesc.Description;
                tmpS.OCasePack = (short)_cost.Pack.Value;
                tmpS.OCaseCost = (double)_cost.CaseCost.Value;
                tmpS.OQty = (short)_price.BasePrcQty.Value;
                tmpS.ORetail = (double)_price.BasePrice.Value;
                SubsPC.Add(tmpS);
            }
            return SubsPC;
        }

        public IEnumerable<SubmissionTpr> GetTPRDetails(int subID, string vendor)
        {
            return _vendorPortalContext.SubmissionTpr.Where(s => s.SubId == subID && s.VendorId == vendor);
        }

        public IEnumerable<ItemHistoryDTO> GetItemMovement(string upc, DateTime sDt, DateTime eDt)
        {
            var _tmpStores = _bRdataContext.Stores.Where(s => s.Postype == " IS8").Select(s => s.Store);
            List<ItemHistoryDTO> _tmpResult = new List<ItemHistoryDTO>();
            foreach (string store in _tmpStores)
            {
                var tmpHistory = _bRdataContext.ItemMovement;
                var query =
                    from history in tmpHistory
                    where history.Store == store && history.Upc == brUPC(upc) && history.Date >= sDt && history.Date <= eDt
                    group history by history.Store into g
                    select new
                    {
                        totalSold = g.Sum(hist => hist.QtySold),
                        avgRetail = g.Average(hist => (hist.Price / hist.PrcQty)),
                        avgCost = g.Average(hist => (hist.UnitCost))
                    };
                if (query == null)
                {
                    return null;
                }
                foreach (var q in query)
                {
                    ItemHistoryDTO _tmpItem = new ItemHistoryDTO();
                    _tmpItem.Store = store;
                    _tmpItem.UPC = upc;
                    _tmpItem.QuantitySold = (short)q.totalSold.Value;
                    _tmpItem.AverageRetail = q.avgRetail.Value;
                    _tmpItem.AverageGM = (_tmpItem.AverageRetail - q.avgCost.Value) / _tmpItem.AverageRetail;
                    _tmpResult.Add(_tmpItem);
                }
            }
            return _tmpResult;
        }
    }
}