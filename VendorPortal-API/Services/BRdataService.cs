﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using VendorPortal_API.Models.BRdata;
using VendorPortal_API.DTO;
using VendorPortal_API.Mapping;

namespace VendorPortal_API.Services
{
    public interface IBRdataService
    {
        ItemDTO GetItem(string byUPC);
    }

    public class BRdataService : IBRdataService
    {
        private BRdataContext _BRdataContext;
        private IMapItems _mapItems;

        public BRdataService(BRdataContext brDataContext)
        {
            _BRdataContext = brDataContext;
            _mapItems = new MapItems();
        }

        public ItemDTO GetItem(string byUPC)
        {
            byUPC = byUPC.PadLeft(15, '0');
            var _item = _BRdataContext.Item.Where(s => s.Upc == byUPC).SingleOrDefault();
            if (_item == null)
                return null;
            var _price = _BRdataContext.Price.Where(s => s.Store == "00000A" && s.Upc == byUPC).SingleOrDefault();
            if (_price == null)
                return null;
            var _cost = _BRdataContext.VendorCost.Where(s => s.Vendor == _item.Vendor && s.Upc == byUPC).SingleOrDefault();
            if (_cost == null)
                return null;
            return _mapItems.mapToDTO(_item, _price, _cost);
        }
    }
}