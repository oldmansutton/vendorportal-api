﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VendorPortal_API.Models.Users;

namespace VendorPortal_API.Services
{
    public interface IUserService
    {
        Users Authenticate(string email, string password);
        IEnumerable<Users> GetAll();
        Users GetById(int id);
        Users Create(Users user, string password);
        void Update(Users user, string password = null);
        void Delete(int id);
    }

    public class UserService : IUserService
    {
        private UsersContext _usersContext;

        public UserService(UsersContext usersContext)
        {
            _usersContext = usersContext;
        }

        public Users Authenticate(string email, string password)
        {
            // If missing information, return null
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
                return null;
            // set user to record for email address
            var user = _usersContext.Users.SingleOrDefault(x => x.EmailAddress == email);

            // Check if email adddress exists
            if (user == null)
                return null;

            // Check if password is correct
            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return null;

            // Check if the user has been approved yet
            if (!user.Approved)
                return null;

            // Successful authentication
            return user;
        }

        public IEnumerable<Users> GetAll()
        {
            return _usersContext.Users;
        }

        public Users GetById(int id)
        {
            return _usersContext.Users.Find(id);
        }

        public Users Create(Users user, string password)
        {
            // Input Validation
            if (string.IsNullOrWhiteSpace(password))
                throw new ApplicationException("A password is required.");
            if (password.Length < 8)
                throw new ApplicationException("Password is too short, must be 8 characters or more.");
            if (password.Length > 20)
                throw new ApplicationException("Password is too long, must be under 20 characters.");
            if (user.VendorId.Length != 6)
                throw new ApplicationException("Vendor ID should be 6 characters.");
            if (_usersContext.Users.Any(x => x.EmailAddress == user.EmailAddress))
                throw new ApplicationException("User already exists with this e-mail address.");


            // Create a password hash
            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(password, out passwordHash, out passwordSalt);
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
            user.Approved = false;
            user.RoleId = "none";

            // Add the user to database
            _usersContext.Users.Add(user);
            _usersContext.SaveChanges();

            return user;
        }

        public void Update(Users inUser, string password = null)
        {
            var user = _usersContext.Users.Find(inUser.UserId);
            if (user == null)
                throw new ApplicationException("User not found.");

            // User email has changed, check to make sure it's not already taken
            if (inUser.EmailAddress != user.EmailAddress)
            {
                if (_usersContext.Users.Any(x => x.EmailAddress == inUser.EmailAddress))
                    throw new ApplicationException("Email address " + inUser.EmailAddress + " is already registered.");
            }

            // Update password if necessary
            if (!string.IsNullOrWhiteSpace(password))
            {
                if (password.Length < 8)
                    throw new ApplicationException("Password is too short, must be 8 characters or more.");
                if (password.Length > 20)
                    throw new ApplicationException("Password is too long, must be under 20 characters.");
                byte[] passwordHash, passwordSalt;
                CreatePasswordHash(password, out passwordHash, out passwordSalt);
                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
            }

            _usersContext.Users.Update(user);
            _usersContext.SaveChanges();


            // Update user properties
            user.FirstName = inUser.FirstName;
            user.LastName = inUser.LastName;
            user.EmailAddress = inUser.EmailAddress;
        }

        public void Delete(int id)
        {
            var user = _usersContext.Users.Find(id);
            if (user != null)
            {
                _usersContext.Users.Remove(user);
                _usersContext.SaveChanges();
            }
        }

        // Private Hashing Methods
        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null)
                throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null)
                throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64)
                throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128)
                throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordSalt");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i])
                        return false;
                }
            }

            return true;
        }
    }
}
