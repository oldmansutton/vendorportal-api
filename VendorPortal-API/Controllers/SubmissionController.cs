﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using VendorPortal_API.Models.Submissions;
using VendorPortal_API.Services;
using VendorPortal_API.Settings;
using static System.Web.HttpUtility;

namespace VendorPortal_API.Controllers
{

    [ApiController]
    [Route("api/Submissions")]
    public class SubmissionController : ControllerBase
    {
        private const string NO_SUB_DT = "Could not load submission details";
        private const string NO_ITEM_HST = "Could not find movement history for item in date range";
        private const string NO_ITEM_DT = "Could not load item details";

        private ISubmissionService _submissionService;
        private readonly AppSettings _appSettings;

        public SubmissionController(ISubmissionService submissionService, IOptions<AppSettings> appSettings)
        {
            _submissionService = submissionService;
            _appSettings = appSettings.Value;
        }

        // GET: api/Submissions
        [HttpGet]
        [Authorize]
        public IActionResult GetAll()
        {
            var subs = _submissionService.GetAll();
            return Ok(subs);
        }

        // GET: api/Submissions/Pending
        [HttpGet("Pending")]
        [Authorize]
        public IActionResult GetAllPending()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                var userIdClaim = identity.FindFirst("UserID").Value;
                var vendorClaim = identity.FindFirst("VendorID").Value;
                var roleClaim = identity.FindFirst("RoleID").Value;
                var subs = _submissionService.GetAllPending(vendorClaim, roleClaim);
                if (subs != null)
                {
                    return Ok(subs);
                }
                return BadRequest(NO_SUB_DT);
            }
            return Unauthorized();
        }

        // GET: api/Submissions/Approved
        [HttpGet("Approved")]
        [Authorize]
        public IActionResult GetAllApproved()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                var userIdClaim = identity.FindFirst("UserID").Value;
                var vendorClaim = identity.FindFirst("VendorID").Value;
                var roleClaim = identity.FindFirst("RoleID").Value;
                var subs = _submissionService.GetAllApproved(vendorClaim, roleClaim);
                if (subs != null)
                {
                    return Ok(subs);
                }
                return BadRequest(NO_SUB_DT);
            }
            return Unauthorized();
        }

        //GET: api/Submissions/Type/1
        [HttpGet("Type/{id}")]
        [Authorize]
        public IActionResult GetAllType([FromRoute] int id)
        {
            var subs = _submissionService.GetAllType(id);
            return Ok(subs);
        }

        //GET: api/Submissions/Type/New/1
        [HttpGet("Type/New/{id}")]
        [Authorize]
        public IActionResult GetSNIDetails([FromRoute] int id)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                var userIdClaim = identity.FindFirst("UserID").Value;
                var vendorClaim = identity.FindFirst("VendorID").Value;
                var roleClaim = identity.FindFirst("RoleID").Value;
                if (roleClaim == "admin")
                {
                    vendorClaim = _submissionService.GetSubVendor(id);
                }
                var subs = _submissionService.GetSNIDetails(id, vendorClaim);
                if (subs != null)
                {
                    return Ok(subs);
                }
                return BadRequest(NO_SUB_DT);
            }
            return Unauthorized();
        }

        [HttpGet("Type/CP/{id}")]
        [Authorize]
        public IActionResult GetCPDetails([FromRoute] int id)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                var userIdClaim = identity.FindFirst("UserID").Value;
                var vendorClaim = identity.FindFirst("VendorID").Value;
                var roleClaim = identity.FindFirst("RoleID").Value;
                if (roleClaim == "admin")
                {
                    vendorClaim = _submissionService.GetSubVendor(id);
                }
                var subs = _submissionService.GetCPDetails(id, vendorClaim);
                if (subs != null)
                {
                    return Ok(subs);
                }
                return BadRequest(NO_SUB_DT);
            }
            return Unauthorized();
        }

        [HttpGet("Type/TPR/{id}")]
        [Authorize]
        public IActionResult GetTPRDetails([FromRoute] int id)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                var userIdClaim = identity.FindFirst("UserID").Value;
                var vendorClaim = identity.FindFirst("VendorID").Value;
                var roleClaim = identity.FindFirst("RoleID").Value;
                if (roleClaim == "admin")
                {
                    vendorClaim = _submissionService.GetSubVendor(id);
                }
                var subs = _submissionService.GetTPRDetails(id, vendorClaim);
                if (subs != null)
                {
                    return Ok(subs);
                }
                return BadRequest(NO_SUB_DT);
            }
            return Unauthorized();
        }

        [HttpGet("Movement/{id}/{sdt}/{edt}")]
        [Authorize]
        public IActionResult GetItemMovement([FromRoute] string id, string sdt, string edt)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                var userIdClaim = identity.FindFirst("UserID").Value;
                var vendorClaim = identity.FindFirst("VendorID").Value;
                DateTime startDate = DateTime.ParseExact(sdt,"yyyyMMdd",null);
                DateTime endDate = DateTime.ParseExact(edt, "yyyyMMdd", null);
                var history = _submissionService.GetItemMovement(id, startDate, endDate);
                if (history != null)
                {
                    return Ok(history);
                }
                return BadRequest(NO_ITEM_HST);
            }
            return Unauthorized();
        }
    }
}