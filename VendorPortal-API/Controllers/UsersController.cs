﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Identity;
using VendorPortal_API.Models.Users;
using VendorPortal_API.DTO;
using VendorPortal_API.Mapping;
using VendorPortal_API.Services;
using VendorPortal_API.Settings;

namespace VendorPortal_API.Controllers
{
    [ApiController]
    [Route("api/Users")]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;
        private IMapUsers _mapUsers;
        private readonly AppSettings _appSettings;

        public UsersController(IUserService userService, IMapUsers mapUsers, IOptions<AppSettings> appSettings)
        {
            _userService = userService;
            _mapUsers = mapUsers;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("Auth")]
        public IActionResult Authenticate([FromBody]UsersDTO usersDTO)
        {
            var user = _userService.Authenticate(usersDTO.EmailAddress, usersDTO.Password);

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect, or user has not been approved." });

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.UserId.ToString()),
                    new Claim("UserID", user.UserId.ToString()),
                    new Claim("RoleID", user.RoleId),
                    new Claim("VendorID", user.VendorId.ToString()),
                }),
                Expires = DateTime.UtcNow.AddMinutes(30),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            return Ok(new
            {
                UserId = user.UserId,
                FirstName = user.FirstName,
                LastName = user.LastName,
                EmailAddress = user.EmailAddress,
		        RoleID = user.RoleId,
		        VendorID = user.VendorId,
		        Approved = user.Approved,
                Token = tokenString
            });
        }

        // GET: api/Users
        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _userService.GetAll();
            var userDTOs = _mapUsers.mapToDTO<IList<UsersDTO>>(users);
            return Ok(userDTOs);
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public IActionResult GetByID([FromRoute] int id)
        {
            var user = _userService.GetById(id);
            var userDTO = _mapUsers.mapToDTO(user);
            return Ok(userDTO);
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public IActionResult Update([FromRoute] int id, [FromBody] UsersDTO usersDTO)
        {
            var user = _mapUsers.MapFromDTO(usersDTO);
            user.UserId = id;

            try
            {
                _userService.Update(user, usersDTO.Password);
                return Ok( new { message = "Saved account information." });
            }
            catch(ApplicationException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        // POST: api/Users
        [AllowAnonymous]
        [HttpPost("Register")]
        public IActionResult RegisterUser([FromBody] UsersDTO usersDTO)
        {
            Users users = _mapUsers.MapFromDTO(usersDTO);

            try
            {
                _userService.Create(users, usersDTO.Password);
                return Ok(new { message = "Registration was successful, an email will be sent when you have been approved." });
            }
            catch(ApplicationException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public IActionResult Delete([FromRoute] int id)
        {
            _userService.Delete(id);
            return Ok();
        }

    }
}