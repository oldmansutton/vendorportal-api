﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using VendorPortal_API.Services;
using VendorPortal_API.Settings;

namespace VendorPortal_API.Controllers
{
    [Route("api/BRdata")]
    [ApiController]
    public class BRdataController : ControllerBase
    {
        private IBRdataService _BRdataService;
        private readonly AppSettings _appSettings;

        public BRdataController(IBRdataService bRdataService, IOptions<AppSettings> appSettings)
        {
            _BRdataService = bRdataService;
            _appSettings = appSettings.Value;
        }

        // GET: api/BRdata/7025300001
        [HttpGet("{upc}")]
        public IActionResult GetItem([FromRoute] string upc)
        {
            var _result = _BRdataService.GetItem(upc);
            if (_result != null)
            {
                return Ok(_BRdataService.GetItem(upc));
            } else
            {
                return NotFound(new { message = "Could not load the specified record." });
            }
        }
    }
}