﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace VendorPortal_API.Models.Submissions
{
    public partial class VendorPortalContext : DbContext
    {
        public VendorPortalContext()
        {
        }

        public VendorPortalContext(DbContextOptions<VendorPortalContext> options)
            : base(options)
        {
        }

        public virtual DbSet<SubmissionCostPrice> SubmissionCostPrice { get; set; }
        public virtual DbSet<SubmissionNewItem> SubmissionNewItem { get; set; }
        public virtual DbSet<Submissions> Submissions { get; set; }
        public virtual DbSet<SubmissionTpr> SubmissionTpr { get; set; }
        public virtual DbSet<SubmissionTypes> SubmissionTypes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot Configuration = new ConfigurationBuilder()
                    .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                    .AddJsonFile("appsettings.json")
                    .Build();
                optionsBuilder.UseSqlServer(Configuration.GetConnectionString("VendorPortalDatabase"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SubmissionCostPrice>(entity =>
            {
                entity.HasKey(e => new { e.VendorId, e.SubId, e.Upc });

                entity.Property(e => e.VendorId)
                    .HasColumnName("VendorID")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.SubId).HasColumnName("SubID");

                entity.Property(e => e.Upc)
                    .HasColumnName("UPC")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.ItemNewCaseCost).HasColumnName("Item_New_Case_Cost");

                entity.Property(e => e.ItemNewCasePack).HasColumnName("Item_New_Case_Pack");

                entity.Property(e => e.ItemNewSrp).HasColumnName("Item_New_SRP");

                entity.Property(e => e.ItemNewSrpQty).HasColumnName("Item_New_SRP_Qty");

                entity.HasOne(d => d.Submissions)
                    .WithMany(p => p.SubmissionCostPrice)
                    .HasForeignKey(d => new { d.VendorId, d.SubId })
                    .HasConstraintName("FK_tblSubmissionCostPrice_tblSubmissions");
            });

            modelBuilder.Entity<SubmissionNewItem>(entity =>
            {
                entity.HasKey(e => new { e.VendorId, e.SubId, e.Upc });

                entity.Property(e => e.VendorId)
                    .HasColumnName("VendorID")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.SubId).HasColumnName("SubID");

                entity.Property(e => e.Upc)
                    .HasColumnName("UPC")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.ItemCaseCost).HasColumnName("Item_Case_Cost");

                entity.Property(e => e.ItemCasePack).HasColumnName("Item_Case_Pack");

                entity.Property(e => e.ItemDepartment).HasColumnName("Item_Department");

                entity.Property(e => e.ItemDepositId).HasColumnName("Item_Deposit_ID");

                entity.Property(e => e.ItemDescription)
                    .IsRequired()
                    .HasColumnName("Item_Description")
                    .HasMaxLength(36);

                entity.Property(e => e.ItemOrderCode)
                    .HasColumnName("Item_Order_Code")
                    .HasMaxLength(7)
                    .IsUnicode(false);

                entity.Property(e => e.ItemPosDescription)
                    .HasColumnName("Item_POS_Description")
                    .HasMaxLength(22);

                entity.Property(e => e.ItemSize)
                    .IsRequired()
                    .HasColumnName("Item_Size")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.ItemSrp).HasColumnName("Item_SRP");

                entity.Property(e => e.ItemSrpQty).HasColumnName("Item_SRP_Qty");

                entity.Property(e => e.ItemUnitDepositId).HasColumnName("Item_Unit_Deposit_ID");

                entity.HasOne(d => d.Submissions)
                    .WithMany(p => p.SubmissionNewItem)
                    .HasForeignKey(d => new { d.VendorId, d.SubId })
                    .HasConstraintName("FK_tblSubmissionNewItem_tblSubmissions");
            });

            modelBuilder.Entity<Submissions>(entity =>
            {
                entity.HasKey(e => new { e.VendorId, e.SubId });

                entity.Property(e => e.VendorId)
                    .HasColumnName("VendorID")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.SubId)
                    .HasColumnName("SubID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.SubApproved).HasDefaultValueSql("((0))");

                entity.Property(e => e.SubApprovedDate).HasColumnType("datetime");

                entity.Property(e => e.SubApprovedUserId).HasColumnName("SubApprovedUserID");

                entity.Property(e => e.SubDescription)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.SubEndDate).HasColumnType("date");

                entity.Property(e => e.SubLastUpdate).HasColumnType("datetime");

                entity.Property(e => e.SubStartDate).HasColumnType("date");

                entity.Property(e => e.SubSubmittedUserId).HasColumnName("SubSubmittedUserID");

                entity.Property(e => e.SubTypeId).HasColumnName("SubTypeID");

                entity.HasOne(d => d.SubType)
                    .WithMany(p => p.Submissions)
                    .HasForeignKey(d => d.SubTypeId)
                    .HasConstraintName("FK_tblSubmissions_tblSubmissionTypes");
            });

            modelBuilder.Entity<SubmissionTpr>(entity =>
            {
                entity.HasKey(e => new { e.VendorId, e.SubId, e.Upc });

                entity.ToTable("SubmissionTPR");

                entity.Property(e => e.VendorId)
                    .HasColumnName("VendorID")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.SubId).HasColumnName("SubID");

                entity.Property(e => e.Upc)
                    .HasColumnName("UPC")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.AllowanceEndDate)
                    .HasColumnName("Allowance_End_Date")
                    .HasColumnType("date");

                entity.Property(e => e.AllowanceStartDate)
                    .HasColumnName("Allowance_Start_Date")
                    .HasColumnType("date");

                entity.Property(e => e.ScanDownAmount).HasColumnType("money");

                entity.Property(e => e.TprPriceQty).HasColumnName("TPR_Price_Qty");

                entity.Property(e => e.TprPriceRetail).HasColumnName("TPR_Price_Retail");

                entity.HasOne(d => d.Submissions)
                    .WithMany(p => p.SubmissionTpr)
                    .HasForeignKey(d => new { d.VendorId, d.SubId })
                    .HasConstraintName("FK_tblSubmissionTPR_tblSubmissions");
            });

            modelBuilder.Entity<SubmissionTypes>(entity =>
            {
                entity.HasKey(e => e.SubTypeId);

                entity.Property(e => e.SubTypeId).HasColumnName("SubTypeID");

                entity.Property(e => e.SubTypeAssocTable).HasMaxLength(50);

                entity.Property(e => e.SubTypeDesc).HasColumnType("text");

                entity.Property(e => e.SubTypeName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });
        }
    }
}
