﻿namespace VendorPortal_API.Models.Submissions
{
    public partial class SubmissionCostPrice
    {
        public string VendorId { get; set; }
        public int SubId { get; set; }
        public string Upc { get; set; }
        public short ItemNewCasePack { get; set; }
        public double ItemNewCaseCost { get; set; }
        public short ItemNewSrpQty { get; set; }
        public double ItemNewSrp { get; set; }

        public Submissions Submissions { get; set; }
    }
}
