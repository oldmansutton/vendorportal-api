﻿using System;
using System.Collections.Generic;

namespace VendorPortal_API.Models.Submissions
{
    public partial class SubmissionTpr
    {
        public string VendorId { get; set; }
        public int SubId { get; set; }
        public string Upc { get; set; }
        public double Allowance { get; set; }
        public DateTime? AllowanceStartDate { get; set; }
        public DateTime? AllowanceEndDate { get; set; }
        public decimal? ScanDownAmount { get; set; }
        public short TprPriceQty { get; set; }
        public double TprPriceRetail { get; set; }

        public Submissions Submissions { get; set; }
    }
}
