﻿using System;
using System.Collections.Generic;

namespace VendorPortal_API.Models.Submissions
{
    public partial class Submissions
    {
        public Submissions()
        {
            SubmissionCostPrice = new HashSet<SubmissionCostPrice>();
            SubmissionNewItem = new HashSet<SubmissionNewItem>();
            SubmissionTpr = new HashSet<SubmissionTpr>();
        }

        public string VendorId { get; set; }
        public int SubId { get; set; }
        public string SubDescription { get; set; }
        public DateTime SubStartDate { get; set; }
        public DateTime SubEndDate { get; set; }
        public DateTime? SubApprovedDate { get; set; }
        public bool? SubApproved { get; set; }
        public DateTime? SubLastUpdate { get; set; }
        public int? SubSubmittedUserId { get; set; }
        public int? SubApprovedUserId { get; set; }
        public int SubTypeId { get; set; }

        public SubmissionTypes SubType { get; set; }
        public ICollection<SubmissionCostPrice> SubmissionCostPrice { get; set; }
        public ICollection<SubmissionNewItem> SubmissionNewItem { get; set; }
        public ICollection<SubmissionTpr> SubmissionTpr { get; set; }
    }
}
