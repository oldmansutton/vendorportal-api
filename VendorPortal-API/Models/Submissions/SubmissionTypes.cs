﻿using System;
using System.Collections.Generic;

namespace VendorPortal_API.Models.Submissions
{
    public partial class SubmissionTypes
    {
        public SubmissionTypes()
        {
            Submissions = new HashSet<Submissions>();
        }

        public int SubTypeId { get; set; }
        public string SubTypeName { get; set; }
        public string SubTypeAssocTable { get; set; }
        public string SubTypeDesc { get; set; }

        public ICollection<Submissions> Submissions { get; set; }
    }
}
