﻿using System;
using System.Collections.Generic;

namespace VendorPortal_API.Models.Submissions
{
    public partial class SubmissionNewItem
    {
        public string VendorId { get; set; }
        public int SubId { get; set; }
        public string Upc { get; set; }
        public string ItemOrderCode { get; set; }
        public string ItemDescription { get; set; }
        public string ItemPosDescription { get; set; }
        public short ItemDepartment { get; set; }
        public short ItemCasePack { get; set; }
        public double ItemCaseCost { get; set; }
        public short ItemSrpQty { get; set; }
        public double ItemSrp { get; set; }
        public string ItemSize { get; set; }
        public double? ItemDepositId { get; set; }
        public double? ItemUnitDepositId { get; set; }

        public Submissions Submissions { get; set; }
    }
}
