﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace VendorPortal_API.Models.BRdata
{
    public partial class BRdataContext : DbContext
    {
        public BRdataContext()
        {
        }

        public BRdataContext(DbContextOptions<BRdataContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Item> Item { get; set; }
        public virtual DbSet<ItemMovement> ItemMovement { get; set; }
        public virtual DbSet<MajorDept> MajorDept { get; set; }
        public virtual DbSet<Price> Price { get; set; }
        public virtual DbSet<Stores> Stores { get; set; }
        public virtual DbSet<VendorCost> VendorCost { get; set; }
        public virtual DbSet<Vendors> Vendors { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=BOSS\\SQLEXPRESS;Database=BRdata;User ID=toms;Password=user");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Item>(entity =>
            {
                entity.HasKey(e => e.Upc);

                entity.HasIndex(e => e.Category)
                    .HasName("IXItemCategory");

                entity.HasIndex(e => e.Department)
                    .HasName("IXItemDept");

                entity.HasIndex(e => e.LinkCode)
                    .HasName("IXItemLink");

                entity.Property(e => e.Upc)
                    .HasColumnName("UPC")
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AcsitemType)
                    .HasColumnName("ACSItemType")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.AcspricingMethod)
                    .HasColumnName("ACSPricingMethod")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.AdvertisingBrand).HasMaxLength(15);

                entity.Property(e => e.AdvertisingDescription).HasMaxLength(35);

                entity.Property(e => e.AdvertisingFlavor).HasMaxLength(25);

                entity.Property(e => e.AisleNum)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.AuditDate).HasColumnType("date");

                entity.Property(e => e.AuditItemCodeKey)
                    .HasMaxLength(7)
                    .IsUnicode(false);

                entity.Property(e => e.AuditType)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.AutoTareCode)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Brand).HasMaxLength(15);

                entity.Property(e => e.BreakerCode)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.CasePricePctDisc).HasColumnType("decimal(7, 3)");

                entity.Property(e => e.CaseUpc)
                    .HasColumnName("CaseUPC")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.CaseWeight).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.Category)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Cgoitem).HasColumnName("CGOItem");

                entity.Property(e => e.ClassCode)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Commodity)
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.CompareAndSaveUpc)
                    .HasColumnName("CompareAndSaveUPC")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.CouponInd)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CouponValue).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.CrossDockVendor)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.CubeSize).HasColumnType("decimal(6, 3)");

                entity.Property(e => e.CubeWeight).HasColumnType("decimal(7, 3)");

                entity.Property(e => e.DepositAmt).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.Depth).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.Description).HasMaxLength(36);

                entity.Property(e => e.DiscontinuedCode)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DiscontinuedDate).HasColumnType("date");

                entity.Property(e => e.DoNotGenerateTpr).HasColumnName("DoNotGenerateTPR");

                entity.Property(e => e.Dsditem).HasColumnName("DSDItem");

                entity.Property(e => e.Eslitem).HasColumnName("ESLItem");

                entity.Property(e => e.EthnicityCode)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Fsaitem).HasColumnName("FSAItem");

                entity.Property(e => e.FxitemType)
                    .HasColumnName("FXItemType")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.FxpricingMethod)
                    .HasColumnName("FXPricingMethod")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Height).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.IbmitemType).HasColumnName("IBMItemType");

                entity.Property(e => e.InternetCategory)
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.ItemLocation)
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.ItemSensitivity)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LabelCopies)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.LastDateSold).HasColumnType("date");

                entity.Property(e => e.LastUpdated).HasColumnType("smalldatetime");

                entity.Property(e => e.LinkCode)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Merchandiser)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.MixMatch)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Msrp)
                    .HasColumnName("MSRP")
                    .HasColumnType("decimal(6, 2)");

                entity.Property(e => e.Msrpqty).HasColumnName("MSRPQty");

                entity.Property(e => e.NuVal)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.OrderBookIndex)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.OutOfStockDate).HasColumnType("date");

                entity.Property(e => e.PalletRoundPercent).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.PosageVerification)
                    .HasColumnName("POSAgeVerification")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.PosalcoholContent)
                    .HasColumnName("POSAlcoholContent")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.PosalcoholType)
                    .HasColumnName("POSAlcoholType")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PosaltDept).HasColumnName("POSAltDept");

                entity.Property(e => e.PosaltPromoCode)
                    .HasColumnName("POSAltPromoCode")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.PosbigCouponLimit).HasColumnName("POSBigCouponLimit");

                entity.Property(e => e.Poschargeable).HasColumnName("POSChargeable");

                entity.Property(e => e.PoscostPlus).HasColumnName("POSCostPlus");

                entity.Property(e => e.PoscouponItem)
                    .HasColumnName("POSCouponItem")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PoscouponWeightLimit)
                    .HasColumnName("POSCouponWeightLimit")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PoscpnMultNotAllowed).HasColumnName("POSCpnMultNotAllowed");

                entity.Property(e => e.PoscrossPromotion)
                    .HasColumnName("POSCrossPromotion")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PoscurrentFamily)
                    .HasColumnName("POSCurrentFamily")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Posdeagroups)
                    .HasColumnName("POSDEAGroups")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.PosdecimalQty).HasColumnName("POSDecimalQty");

                entity.Property(e => e.Posdescription)
                    .HasColumnName("POSDescription")
                    .HasMaxLength(22);

                entity.Property(e => e.Posdescription28)
                    .HasColumnName("POSDescription28")
                    .HasMaxLength(28);

                entity.Property(e => e.PosdiscountNum)
                    .HasColumnName("POSDiscountNum")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Posdiscountable).HasColumnName("POSDiscountable");

                entity.Property(e => e.PosexcludeFromPromo).HasColumnName("POSExcludeFromPromo");

                entity.Property(e => e.PosfamilyNum)
                    .HasColumnName("POSFamilyNum")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.PosgreenpointsSpecial).HasColumnName("POSGreenpointsSpecial");

                entity.Property(e => e.PosgrillPrintItem).HasColumnName("POSGrillPrintItem");

                entity.Property(e => e.PosgroupList)
                    .HasColumnName("POSGroupList")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Posifpscode)
                    .HasColumnName("POSIFPSCode")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.PosinfoPilotPromotions)
                    .HasColumnName("POSInfoPilotPromotions")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.PositemRestrictions)
                    .HasColumnName("POSItemRestrictions")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Positemizer5).HasColumnName("POSItemizer5");

                entity.Property(e => e.Positemizer6).HasColumnName("POSItemizer6");

                entity.Property(e => e.PositemsToValidateCoupon).HasColumnName("POSItemsToValidateCoupon");

                entity.Property(e => e.PoslargeLink)
                    .HasColumnName("POSLargeLink")
                    .HasMaxLength(14)
                    .IsUnicode(false);

                entity.Property(e => e.PoslinkCode).HasColumnName("POSLinkCode");

                entity.Property(e => e.PoslinkToDeposit)
                    .HasColumnName("POSLinkToDeposit")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PosmanufCouponNum)
                    .HasColumnName("POSManufCouponNum")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.PosmaxNumOfCoupons).HasColumnName("POSMaxNumOfCoupons");

                entity.Property(e => e.PosminPurchaseAmt)
                    .HasColumnName("POSMinPurchaseAmt")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.PosmultPriceGroup).HasColumnName("POSMultPriceGroup");

                entity.Property(e => e.PosnegativeEntry).HasColumnName("POSNegativeEntry");

                entity.Property(e => e.PosnonmerchandiseSale).HasColumnName("POSNonmerchandiseSale");

                entity.Property(e => e.PospointsApply).HasColumnName("POSPointsApply");

                entity.Property(e => e.PospointsOnlyItem).HasColumnName("POSPointsOnlyItem");

                entity.Property(e => e.PospreviousFamily)
                    .HasColumnName("POSPreviousFamily")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.PospriceRequired).HasColumnName("POSPriceRequired");

                entity.Property(e => e.PospricingMethod).HasColumnName("POSPricingMethod");

                entity.Property(e => e.PosprintItem).HasColumnName("POSPrintItem");

                entity.Property(e => e.PosprohibitGroupListing).HasColumnName("POSProhibitGroupListing");

                entity.Property(e => e.PosprohibitManualItemDiscount).HasColumnName("POSProhibitManualItemDiscount");

                entity.Property(e => e.PosprohibitMinOrderAccumulation).HasColumnName("POSProhibitMinOrderAccumulation");

                entity.Property(e => e.PosprohibitQty).HasColumnName("POSProhibitQty");

                entity.Property(e => e.PosprohibitRefund).HasColumnName("POSProhibitRefund");

                entity.Property(e => e.PosprohibitRepeat).HasColumnName("POSProhibitRepeat");

                entity.Property(e => e.PosprohibitReturn).HasColumnName("POSProhibitReturn");

                entity.Property(e => e.PospromoCode).HasColumnName("POSPromoCode");

                entity.Property(e => e.PosqtyAllowed).HasColumnName("POSQtyAllowed");

                entity.Property(e => e.PosqtyRequired).HasColumnName("POSQtyRequired");

                entity.Property(e => e.PosrecalledItem).HasColumnName("POSRecalledItem");

                entity.Property(e => e.Posrestricted).HasColumnName("POSRestricted");

                entity.Property(e => e.PosrestrictedSaleType)
                    .HasColumnName("POSRestrictedSaleType")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.PosrestrictionLayout)
                    .HasColumnName("POSRestrictionLayout")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.PosreturnCode)
                    .HasColumnName("POSReturnCode")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.PosseparateMinCoupon).HasColumnName("POSSeparateMinCoupon");

                entity.Property(e => e.PosserviceCharge).HasColumnName("POSServiceCharge");

                entity.Property(e => e.PosterminalItem).HasColumnName("POSTerminalItem");

                entity.Property(e => e.PostitemMessage)
                    .HasColumnName("POSTItemMessage")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.PosuserOption4).HasColumnName("POSUserOption4");

                entity.Property(e => e.PosvalueCardState)
                    .HasColumnName("POSValueCardState")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PosvalueCardType)
                    .HasColumnName("POSValueCardType")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PosverifyAgeAlcohol).HasColumnName("POSVerifyAgeAlcohol");

                entity.Property(e => e.PosverifyAgeTobacco).HasColumnName("POSVerifyAgeTobacco");

                entity.Property(e => e.PrivateLabelCode)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PrivateLabelStore)
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.PrivateLabelTemplate)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.PromotionInvQtyFromDate).HasColumnType("date");

                entity.Property(e => e.PromotionInvQtyToDate).HasColumnType("date");

                entity.Property(e => e.ReceivingCaseUpc)
                    .HasColumnName("ReceivingCaseUPC")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.ReportCode)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.RetailUnitPackage)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SeasonalCode)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ShelfFactor).HasColumnType("decimal(4, 1)");

                entity.Property(e => e.ShelfNum)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.SizeAlpha)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.SizeDescription)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.SmadjectiveCode)
                    .HasColumnName("SMAdjectiveCode")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Smadlevel)
                    .HasColumnName("SMADLevel")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SmbonusPoints)
                    .HasColumnName("SMBonusPoints")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.SmbottleLink)
                    .HasColumnName("SMBottleLink")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SmbundleCode)
                    .HasColumnName("SMBundleCode")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.SmcouponCode)
                    .HasColumnName("SMCouponCode")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Smdiscount1)
                    .HasColumnName("SMDiscount1")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Smdiscount2)
                    .HasColumnName("SMDiscount2")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SmenforceQty)
                    .HasColumnName("SMEnforceQty")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SmexcludeMinPurchase).HasColumnName("SMExcludeMinPurchase");

                entity.Property(e => e.SmfamilyCode1)
                    .HasColumnName("SMFamilyCode1")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.SmfamilyCode2)
                    .HasColumnName("SMFamilyCode2")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.SmfreeItem).HasColumnName("SMFreeItem");

                entity.Property(e => e.SmfrequentShopper).HasColumnName("SMFrequentShopper");

                entity.Property(e => e.SmgiftCard).HasColumnName("SMGiftCard");

                entity.Property(e => e.SmitemType)
                    .HasColumnName("SMItemType")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SmmixMatch)
                    .HasColumnName("SMMixMatch")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Smpoints)
                    .HasColumnName("SMPoints")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.SmreportCode)
                    .HasColumnName("SMReportCode")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.SmrestrictedSales).HasColumnName("SMRestrictedSales");

                entity.Property(e => e.SmsubDept).HasColumnName("SMSubDept");

                entity.Property(e => e.SmvisualVerify).HasColumnName("SMVisualVerify");

                entity.Property(e => e.Smweight)
                    .HasColumnName("SMWeight")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.Source)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.Property(e => e.TargetGm)
                    .HasColumnName("TargetGM")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.TaxClassCode)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.TpronhandQty).HasColumnName("TPROnhandQty");

                entity.Property(e => e.TprpriceLocked).HasColumnName("TPRPriceLocked");

                entity.Property(e => e.TprthresholdQty).HasColumnName("TPRThresholdQty");

                entity.Property(e => e.UnitDescription)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.UnitOfMeasureCode)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.UnitPriceCompareUpc)
                    .HasColumnName("UnitPriceCompareUPC")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.UnitSize).HasColumnType("decimal(7, 3)");

                entity.Property(e => e.UserDefinedAmt1).HasColumnType("decimal(11, 2)");

                entity.Property(e => e.UserDefinedAmt2).HasColumnType("decimal(11, 2)");

                entity.Property(e => e.UserDefinedAmt3).HasColumnType("decimal(11, 2)");

                entity.Property(e => e.UserDefinedCode1)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.UserDefinedCode10)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.UserDefinedCode11)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.UserDefinedCode12)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.UserDefinedCode13)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.UserDefinedCode14)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.UserDefinedCode15)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.UserDefinedCode2)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.UserDefinedCode3)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.UserDefinedCode4)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.UserDefinedCode5)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.UserDefinedCode6)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.UserDefinedCode7)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.UserDefinedCode8)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.UserDefinedCode9)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.UserDefinedCost1).HasColumnType("decimal(9, 4)");

                entity.Property(e => e.UserDefinedCost2).HasColumnType("decimal(9, 4)");

                entity.Property(e => e.UserDefinedCost3).HasColumnType("decimal(9, 4)");

                entity.Property(e => e.UserDefinedCost4).HasColumnType("decimal(9, 4)");

                entity.Property(e => e.UserDefinedCost5).HasColumnType("decimal(9, 4)");

                entity.Property(e => e.UserDefinedCost6).HasColumnType("decimal(9, 4)");

                entity.Property(e => e.UserDefinedCost7).HasColumnType("decimal(9, 4)");

                entity.Property(e => e.UserDefinedDate1).HasColumnType("date");

                entity.Property(e => e.UserDefinedDate2).HasColumnType("date");

                entity.Property(e => e.UserDefinedDate3).HasColumnType("date");

                entity.Property(e => e.UserDefinedDate4).HasColumnType("date");

                entity.Property(e => e.UserDefinedDate5).HasColumnType("date");

                entity.Property(e => e.UserDefinedDescription1).HasMaxLength(40);

                entity.Property(e => e.UserDefinedDescription2).HasMaxLength(40);

                entity.Property(e => e.UserDefinedDescription3).HasMaxLength(40);

                entity.Property(e => e.UserDefinedDescription4).HasMaxLength(40);

                entity.Property(e => e.UserDefinedDescription5).HasMaxLength(40);

                entity.Property(e => e.VariableWgtCode)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Vendor)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.WarehouseHi).HasColumnName("WarehouseHI");

                entity.Property(e => e.WarehouseTi).HasColumnName("WarehouseTI");

                entity.Property(e => e.WeightType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Whaisle)
                    .HasColumnName("WHAisle")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Wharea)
                    .HasColumnName("WHArea")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Whitem)
                    .HasColumnName("WHItem")
                    .HasMaxLength(7)
                    .IsUnicode(false);

                entity.Property(e => e.WhreservedAisle)
                    .HasColumnName("WHReservedAisle")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.WhreservedArea)
                    .HasColumnName("WHReservedArea")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.WhreservedSlot)
                    .HasColumnName("WHReservedSlot")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Whslot)
                    .HasColumnName("WHSlot")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Wiccvv).HasColumnName("WICCVV");

                entity.Property(e => e.Wicitem).HasColumnName("WICItem");

                entity.Property(e => e.Width).HasColumnType("decimal(5, 2)");
            });

            modelBuilder.Entity<ItemMovement>(entity =>
            {
                entity.HasKey(e => new { e.Store, e.Upc, e.Date, e.Priority });

                entity.HasIndex(e => e.Date)
                    .HasName("IX_ItemMovementD");

                entity.Property(e => e.Store)
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.Upc)
                    .HasColumnName("UPC")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.AmountSold).HasColumnType("decimal(8, 2)");

                entity.Property(e => e.CouponBvalue)
                    .HasColumnName("CouponBValue")
                    .HasColumnType("decimal(6, 2)");

                entity.Property(e => e.CouponType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LastUpdated).HasColumnType("smalldatetime");

                entity.Property(e => e.NetUnitCost).HasColumnType("decimal(6, 2)");

                entity.Property(e => e.Price).HasColumnType("decimal(6, 2)");

                entity.Property(e => e.PriceType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.UnitCost).HasColumnType("decimal(6, 2)");

                entity.Property(e => e.WgtSold).HasColumnType("decimal(7, 2)");
            });

            modelBuilder.Entity<MajorDept>(entity =>
            {
                entity.HasKey(e => e.MajorDept1);

                entity.Property(e => e.MajorDept1)
                    .HasColumnName("MajorDept")
                    .ValueGeneratedNever();

                entity.Property(e => e.Ar).HasColumnName("AR");

                entity.Property(e => e.DepositCrvglcode)
                    .HasColumnName("DepositCRVGLCode")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.DeptGroup)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.DeptRingUpc)
                    .HasColumnName("DeptRingUPC")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.DeptXref)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Description).HasMaxLength(29);

                entity.Property(e => e.ExcludeAdfromPromo).HasColumnName("ExcludeADFromPromo");

                entity.Property(e => e.ExcludeTprfromPromo).HasColumnName("ExcludeTPRFromPromo");

                entity.Property(e => e.GenCostOnPoimp).HasColumnName("GenCostOnPOImp");

                entity.Property(e => e.Glcode)
                    .HasColumnName("GLCode")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.InhouseWhvendor)
                    .HasColumnName("InhouseWHVendor")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.LargeGlcode)
                    .HasColumnName("LargeGLCode")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.LastUpdated).HasColumnType("smalldatetime");

                entity.Property(e => e.NoTprgen).HasColumnName("NoTPRGen");

                entity.Property(e => e.PosageAudit)
                    .HasColumnName("POSAgeAudit")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.PosscanDept).HasColumnName("POSScanDept");

                entity.Property(e => e.SkipWhimport).HasColumnName("SkipWHImport");

                entity.Property(e => e.SmscanDept).HasColumnName("SMScanDept");

                entity.Property(e => e.UpchargePercent).HasColumnType("decimal(5, 2)");
            });

            modelBuilder.Entity<Price>(entity =>
            {
                entity.HasKey(e => new { e.Upc, e.Store });

                entity.Property(e => e.Upc)
                    .HasColumnName("UPC")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Store)
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.AdEndDate).HasColumnType("date");

                entity.Property(e => e.AdPrice).HasColumnType("decimal(8, 2)");

                entity.Property(e => e.AdProfile)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.AdPromoId).HasColumnName("AdPromoID");

                entity.Property(e => e.AdStartDate).HasColumnType("date");

                entity.Property(e => e.BaseEffectiveDate).HasColumnType("date");

                entity.Property(e => e.BasePrice).HasColumnType("decimal(8, 2)");

                entity.Property(e => e.CouponType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DealEndDate).HasColumnType("date");

                entity.Property(e => e.DealPrice).HasColumnType("decimal(8, 2)");

                entity.Property(e => e.DealProfile)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.DealPromoId).HasColumnName("DealPromoID");

                entity.Property(e => e.DealStartDate).HasColumnType("date");

                entity.Property(e => e.DiscountType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Frq2EndDate).HasColumnType("date");

                entity.Property(e => e.Frq2LargeLink)
                    .HasMaxLength(14)
                    .IsUnicode(false);

                entity.Property(e => e.Frq2Price).HasColumnType("decimal(8, 2)");

                entity.Property(e => e.Frq2StartDate).HasColumnType("date");

                entity.Property(e => e.FrqEndDate).HasColumnType("date");

                entity.Property(e => e.FrqLargeLink)
                    .HasMaxLength(14)
                    .IsUnicode(false);

                entity.Property(e => e.FrqPrice).HasColumnType("decimal(8, 2)");

                entity.Property(e => e.FrqProfile)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.FrqPromoId).HasColumnName("FrqPromoID");

                entity.Property(e => e.FrqStartDate).HasColumnType("date");

                entity.Property(e => e.LastCompCheckDate).HasColumnType("date");

                entity.Property(e => e.LastUpdated).HasColumnType("smalldatetime");

                entity.Property(e => e.MinPurchase).HasColumnType("decimal(8, 2)");

                entity.Property(e => e.ScaleTareA).HasColumnType("decimal(7, 3)");

                entity.Property(e => e.ScaleTareB).HasColumnType("decimal(7, 3)");

                entity.Property(e => e.Source)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TprendDate)
                    .HasColumnName("TPREndDate")
                    .HasColumnType("date");

                entity.Property(e => e.TprprcQty).HasColumnName("TPRPrcQty");

                entity.Property(e => e.Tprprice)
                    .HasColumnName("TPRPrice")
                    .HasColumnType("decimal(8, 2)");

                entity.Property(e => e.Tprpriority).HasColumnName("TPRPriority");

                entity.Property(e => e.Tprprofile)
                    .HasColumnName("TPRProfile")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.TprpromoId).HasColumnName("TPRPromoID");

                entity.Property(e => e.TprstartDate)
                    .HasColumnName("TPRStartDate")
                    .HasColumnType("date");

                entity.Property(e => e.Vendor)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.VendorItem)
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Stores>(entity =>
            {
                entity.HasKey(e => e.Store);

                entity.Property(e => e.Store)
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Address1).HasMaxLength(26);

                entity.Property(e => e.Address2).HasMaxLength(26);

                entity.Property(e => e.Address3).HasMaxLength(26);

                entity.Property(e => e.AllianceTpr).HasColumnName("AllianceTPR");

                entity.Property(e => e.Aplusmember).HasColumnName("APLUSMember");

                entity.Property(e => e.BatchSplitType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ChainNum)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.CommType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CompareUpcaisle).HasColumnName("CompareUPCAisle");

                entity.Property(e => e.CorporationNum)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerCode)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.DataManServNm).HasMaxLength(15);

                entity.Property(e => e.DexadjDir)
                    .HasColumnName("DEXAdjDir")
                    .HasMaxLength(40);

                entity.Property(e => e.DisablePoslink).HasColumnName("DisablePOSLink");

                entity.Property(e => e.EshelfLbl).HasColumnName("EShelfLbl");

                entity.Property(e => e.EshelfLblType).HasColumnName("EShelfLblType");

                entity.Property(e => e.Evend).HasColumnName("EVend");

                entity.Property(e => e.ExciseTax)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ExpAcssil).HasColumnName("ExpACSSil");

                entity.Property(e => e.ExportAce169).HasColumnName("ExportACE169");

                entity.Property(e => e.ExportCoste).HasColumnName("ExportCOSTE");

                entity.Property(e => e.ExportFile)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ExportLbl).HasColumnName("ExportLBL");

                entity.Property(e => e.ExportMemPromos)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ExportPprtoBrd).HasColumnName("ExportPPRtoBRD");

                entity.Property(e => e.ExportSnh).HasColumnName("ExportSNH");

                entity.Property(e => e.ExportTprs).HasColumnName("ExportTPRs");

                entity.Property(e => e.ExportVndo).HasColumnName("ExportVNDO");

                entity.Property(e => e.Fax).HasMaxLength(30);

                entity.Property(e => e.FaxFormat).HasMaxLength(30);

                entity.Property(e => e.Following)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ForceItlkbtch).HasColumnName("ForceITLKBtch");

                entity.Property(e => e.FtplogIn)
                    .HasColumnName("FTPLogIn")
                    .HasMaxLength(26);

                entity.Property(e => e.Ftppassword)
                    .HasColumnName("FTPPassword")
                    .HasMaxLength(26);

                entity.Property(e => e.GenTprwhstore).HasColumnName("GenTPRWHStore");

                entity.Property(e => e.Glcode)
                    .HasColumnName("GLCode")
                    .HasMaxLength(15);

                entity.Property(e => e.HostIp)
                    .HasColumnName("HostIP")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.HostPort)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.HostRoot)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IbmmoveDat).HasColumnName("IBMMoveDAT");

                entity.Property(e => e.Ibmvaluepack).HasColumnName("IBMValuepack");

                entity.Property(e => e.ImpBrdmove).HasColumnName("ImpBRDMove");

                entity.Property(e => e.ImpCsvmove).HasColumnName("ImpCSVMove");

                entity.Property(e => e.ImportIbmtlog).HasColumnName("ImportIBMTlog");

                entity.Property(e => e.IntercompanyVendor)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Iss45inS4s).HasColumnName("ISS45InS4S");

                entity.Property(e => e.LandedCostStore)
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.LastImageFile)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.LastMoveDate).HasColumnType("date");

                entity.Property(e => e.LastUpdated).HasColumnType("smalldatetime");

                entity.Property(e => e.LblTempStore)
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.LocalDataDir).HasMaxLength(50);

                entity.Property(e => e.LockExport)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.LoqinSil).HasColumnName("LOQinSil");

                entity.Property(e => e.MasterStore)
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(26);

                entity.Property(e => e.NewItemAuth)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.NoCopyfileBs).HasColumnName("NoCopyfileBS");

                entity.Property(e => e.OpenPosviaIp).HasColumnName("OpenPOSViaIP");

                entity.Property(e => e.OrdWhstore)
                    .HasColumnName("OrdWHStore")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.OvrDefaultWh).HasColumnName("OvrDefaultWH");

                entity.Property(e => e.Phone).HasMaxLength(30);

                entity.Property(e => e.PhoneFormat).HasMaxLength(30);

                entity.Property(e => e.PlumcloneStore)
                    .HasColumnName("PLUMCloneStore")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.Posdest)
                    .HasColumnName("POSDest")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Pospath)
                    .HasColumnName("POSPath")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PosstoreIp)
                    .HasColumnName("POSStoreIP")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Postype)
                    .HasColumnName("POSType")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.PpbatchNum)
                    .HasColumnName("PPBatchNum")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Pprstore)
                    .HasColumnName("PPRStore")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.PrcOptRule)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.ProratePercent).HasColumnType("decimal(6, 3)");

                entity.Property(e => e.RboinSil).HasColumnName("RBOInSil");

                entity.Property(e => e.RootDrive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.RoundingRule)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.ScaleGroup)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.ScaleType)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.ScanDriver).HasMaxLength(64);

                entity.Property(e => e.SmallTagTemplate)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Smdirect).HasColumnName("SMDirect");

                entity.Property(e => e.Store3)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Store4)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.StoreExpZone)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.StoreIp)
                    .HasColumnName("StoreIP")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.StoreIpport)
                    .HasColumnName("StoreIPPort")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.StorePonum).HasColumnName("StorePONum");

                entity.Property(e => e.Tciexport).HasColumnName("TCIExport");

                entity.Property(e => e.UseStoreCpt).HasColumnName("UseStoreCPT");

                entity.Property(e => e.Vendor)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Whstore).HasColumnName("WHStore");

                entity.Property(e => e.Zone)
                    .HasMaxLength(6)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VendorCost>(entity =>
            {
                entity.HasKey(e => new { e.Upc, e.Vendor, e.Seq });

                entity.Property(e => e.Upc)
                    .HasColumnName("UPC")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Vendor)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.CaseCost).HasColumnType("decimal(9, 4)");

                entity.Property(e => e.CaseCostNoFrt).HasColumnType("decimal(9, 4)");

                entity.Property(e => e.CaseUpc)
                    .HasColumnName("CaseUPC")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Description).HasMaxLength(30);

                entity.Property(e => e.DiscoCode)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DiscoDate).HasColumnType("date");

                entity.Property(e => e.FrtCostPerLb)
                    .HasColumnName("FrtCostPerLB")
                    .HasColumnType("decimal(9, 4)");

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.LastPurchaseDate).HasColumnType("date");

                entity.Property(e => e.LastUpdated).HasColumnType("smalldatetime");

                entity.Property(e => e.MaxOq).HasColumnName("MaxOQ");

                entity.Property(e => e.Moq).HasColumnName("MOQ");

                entity.Property(e => e.PackType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SizeAlpha)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.Property(e => e.SubVendor)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.SubVendorItem)
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.UnitCost).HasColumnType("decimal(9, 4)");

                entity.Property(e => e.VendorItem)
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Vendors>(entity =>
            {
                entity.HasKey(e => e.Vendor);

                entity.Property(e => e.Vendor)
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Address1).HasMaxLength(30);

                entity.Property(e => e.Address2).HasMaxLength(30);

                entity.Property(e => e.Address4).HasMaxLength(30);

                entity.Property(e => e.Address5).HasMaxLength(30);

                entity.Property(e => e.AltVendor)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.BarCodeType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Broker)
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.Buyer)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.City).HasMaxLength(30);

                entity.Property(e => e.CommCode)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Contact).HasMaxLength(26);

                entity.Property(e => e.DefaultReason)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.DexCommCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DexDunsNum)
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.Disc1).HasColumnType("decimal(6, 3)");

                entity.Property(e => e.Disc2).HasColumnType("decimal(4, 1)");

                entity.Property(e => e.Disc3).HasColumnType("decimal(4, 1)");

                entity.Property(e => e.DiscCredits)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DiscInDsddiscrp).HasColumnName("DiscInDSDDiscrp");

                entity.Property(e => e.DiscPercent).HasColumnType("decimal(4, 1)");

                entity.Property(e => e.DiscType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DiscrpAmt).HasColumnType("decimal(4, 2)");

                entity.Property(e => e.DiscrpType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DsdprintSort)
                    .HasColumnName("DSDPrintSort")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.EslprCpre).HasColumnName("ESLPrCPre");

                entity.Property(e => e.Fax).HasMaxLength(30);

                entity.Property(e => e.FaxFormat).HasMaxLength(30);

                entity.Property(e => e.FedId)
                    .HasColumnName("FedID")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.FlatFrtAmt).HasColumnType("decimal(8, 2)");

                entity.Property(e => e.FlatPuDsddiscrp).HasColumnName("FlatPuDSDDiscrp");

                entity.Property(e => e.FrtInDsddiscrp).HasColumnName("FrtInDSDDiscrp");

                entity.Property(e => e.Glcode)
                    .HasColumnName("GLCode")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.LastUpdated).HasColumnType("smalldatetime");

                entity.Property(e => e.MarkupVendor)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.MaxOrderLevel).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.Message).HasMaxLength(15);

                entity.Property(e => e.MinOrderLevel).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.MiscModeRetAmtRetGm).HasColumnName("MiscModeRetAmtRetGM");

                entity.Property(e => e.OrderFormat)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PaydateInd)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Phone).HasMaxLength(30);

                entity.Property(e => e.PhoneFormat).HasMaxLength(30);

                entity.Property(e => e.PrimaryBillVend)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.RcptDefaultMode)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.RemitVendor)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Rfalwdays).HasColumnName("RFALWDays");

                entity.Property(e => e.RfdefaultCases).HasColumnName("RFDefaultCases");

                entity.Property(e => e.RtlMrkupPct).HasColumnType("decimal(6, 3)");

                entity.Property(e => e.SalesmanCode)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.SkipApapply).HasColumnName("SkipAPApply");

                entity.Property(e => e.State)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.TotalToUse)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TotalsOnlyRecDept)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Tprrule)
                    .HasColumnName("TPRRule")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.TransferVendor)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.UpcchkDig).HasColumnName("UPCChkDig");

                entity.Property(e => e.UpdateCd).HasColumnName("UpdateCD");

                entity.Property(e => e.VendAddedDateDay)
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.VendReclaimCode)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.VendorName).HasMaxLength(30);

                entity.Property(e => e.VendorType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.XrefVendor)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });
        }
    }
}
