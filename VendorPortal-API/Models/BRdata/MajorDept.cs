﻿using System;
using System.Collections.Generic;

namespace VendorPortal_API.Models.BRdata
{
    public partial class MajorDept
    {
        public short MajorDept1 { get; set; }
        public string Description { get; set; }
        public string Glcode { get; set; }
        public bool? SkipInvReport { get; set; }
        public bool? SkipHostCompare { get; set; }
        public byte? PosscanDept { get; set; }
        public byte? SmscanDept { get; set; }
        public bool? TaxA { get; set; }
        public bool? TaxB { get; set; }
        public bool? TaxC { get; set; }
        public bool? TaxD { get; set; }
        public bool? TaxE { get; set; }
        public bool? TaxF { get; set; }
        public bool? TaxG { get; set; }
        public bool? TaxH { get; set; }
        public bool? Foodstamp { get; set; }
        public byte? ScaleDept { get; set; }
        public short? InvoicedDept { get; set; }
        public bool? Discountable { get; set; }
        public string DeptRingUpc { get; set; }
        public string InhouseWhvendor { get; set; }
        public bool? ExclMvmt { get; set; }
        public bool? Ar { get; set; }
        public bool? NoTprgen { get; set; }
        public bool? NonRetail { get; set; }
        public string PosageAudit { get; set; }
        public string DepositCrvglcode { get; set; }
        public bool? SkipWhimport { get; set; }
        public string DeptGroup { get; set; }
        public decimal? UpchargePercent { get; set; }
        public string DeptXref { get; set; }
        public bool? NonMerchandiseItem { get; set; }
        public bool? GenCostOnPoimp { get; set; }
        public bool? AllowOrdZeroInv { get; set; }
        public string LargeGlcode { get; set; }
        public bool? ExcludeAdfromPromo { get; set; }
        public bool? ExcludeTprfromPromo { get; set; }
        public DateTime? LastUpdated { get; set; }
    }
}
