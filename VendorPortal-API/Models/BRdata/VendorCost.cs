﻿using System;
using System.Collections.Generic;

namespace VendorPortal_API.Models.BRdata
{
    public partial class VendorCost
    {
        public string Upc { get; set; }
        public string Vendor { get; set; }
        public int Seq { get; set; }
        public int? ItemId { get; set; }
        public decimal? CaseCost { get; set; }
        public decimal? UnitCost { get; set; }
        public string VendorItem { get; set; }
        public short? Pack { get; set; }
        public bool? MainVendor { get; set; }
        public string PackType { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? DiscoDate { get; set; }
        public string DiscoCode { get; set; }
        public bool? MarketPrice { get; set; }
        public short? CasesInPallet { get; set; }
        public string SubVendorItem { get; set; }
        public string SubVendor { get; set; }
        public string CaseUpc { get; set; }
        public short? ManufacturerPack { get; set; }
        public DateTime? LastPurchaseDate { get; set; }
        public int? SecVendorItem { get; set; }
        public decimal? CaseCostNoFrt { get; set; }
        public decimal? FrtCostPerLb { get; set; }
        public string Description { get; set; }
        public string SizeAlpha { get; set; }
        public short? Moq { get; set; }
        public short? MaxOq { get; set; }
        public DateTime? LastUpdated { get; set; }
    }
}
