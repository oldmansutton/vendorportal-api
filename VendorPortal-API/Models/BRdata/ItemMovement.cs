﻿using System;
using System.Collections.Generic;

namespace VendorPortal_API.Models.BRdata
{
    public partial class ItemMovement
    {
        public string Store { get; set; }
        public string Upc { get; set; }
        public DateTime Date { get; set; }
        public byte Priority { get; set; }
        public int? QtySold { get; set; }
        public decimal? AmountSold { get; set; }
        public decimal? WgtSold { get; set; }
        public decimal? UnitCost { get; set; }
        public decimal? NetUnitCost { get; set; }
        public byte? PrcQty { get; set; }
        public decimal? Price { get; set; }
        public string PriceType { get; set; }
        public string CouponType { get; set; }
        public decimal? CouponBvalue { get; set; }
        public short? Department { get; set; }
        public DateTime? LastUpdated { get; set; }
    }
}
