﻿using System;
using System.Collections.Generic;

namespace VendorPortal_API.Models.BRdata
{
    public partial class Vendors
    {
        public string Vendor { get; set; }
        public string VendorName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Address4 { get; set; }
        public string Address5 { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public string PhoneFormat { get; set; }
        public string AltVendor { get; set; }
        public decimal? DiscPercent { get; set; }
        public short? DiscDays { get; set; }
        public short? NetTermDays { get; set; }
        public string Type { get; set; }
        public int? QtyVoucherNums { get; set; }
        public string Message { get; set; }
        public string Fax { get; set; }
        public string FaxFormat { get; set; }
        public string Contact { get; set; }
        public decimal? Disc1 { get; set; }
        public decimal? Disc2 { get; set; }
        public decimal? Disc3 { get; set; }
        public string OrderFormat { get; set; }
        public string SalesmanCode { get; set; }
        public string DiscType { get; set; }
        public string DiscCredits { get; set; }
        public bool? RfdefaultCases { get; set; }
        public string FedId { get; set; }
        public bool? VendGrpCost { get; set; }
        public bool? Code1099 { get; set; }
        public string MarkupVendor { get; set; }
        public decimal? RtlMrkupPct { get; set; }
        public string Glcode { get; set; }
        public long? CustomerNum { get; set; }
        public string TransferVendor { get; set; }
        public decimal? DiscrpAmt { get; set; }
        public string DiscrpType { get; set; }
        public string CommCode { get; set; }
        public bool? UpdateCd { get; set; }
        public string VendorType { get; set; }
        public string PrimaryBillVend { get; set; }
        public string Buyer { get; set; }
        public bool? Active { get; set; }
        public string RemitVendor { get; set; }
        public bool? TransVendInd { get; set; }
        public decimal? MinOrderLevel { get; set; }
        public decimal? MaxOrderLevel { get; set; }
        public short? CreditDays { get; set; }
        public string XrefVendor { get; set; }
        public string TotalToUse { get; set; }
        public string Tprrule { get; set; }
        public bool? VendorTax { get; set; }
        public byte? Rfalwdays { get; set; }
        public bool? SkipApapply { get; set; }
        public bool? CentralOrder { get; set; }
        public bool? PctAllocFrt { get; set; }
        public string DexCommCode { get; set; }
        public string DexDunsNum { get; set; }
        public string PaydateInd { get; set; }
        public bool? SpecialBarCode { get; set; }
        public byte? CharacterBegin { get; set; }
        public byte? CharacterEnd { get; set; }
        public string BarCodeType { get; set; }
        public bool? DiscInDsddiscrp { get; set; }
        public bool? FrtInDsddiscrp { get; set; }
        public string DsdprintSort { get; set; }
        public short? LeadTime { get; set; }
        public string Broker { get; set; }
        public byte? EslprCpre { get; set; }
        public bool? VmcVendor { get; set; }
        public bool? LandedCost { get; set; }
        public bool? IntercmpVend { get; set; }
        public bool? NoExport { get; set; }
        public bool? FlatPuDsddiscrp { get; set; }
        public bool? GenCostsForAltVend { get; set; }
        public bool? PrintRcptNoPrompt { get; set; }
        public bool? NoClassTableTax { get; set; }
        public string VendAddedDateDay { get; set; }
        public bool? PromptPrtDiscrp { get; set; }
        public string VendReclaimCode { get; set; }
        public decimal? FlatFrtAmt { get; set; }
        public bool? MiscModeRetAmtRetGm { get; set; }
        public string DefaultReason { get; set; }
        public string RcptDefaultMode { get; set; }
        public bool? UpcchkDig { get; set; }
        public string TotalsOnlyRecDept { get; set; }
        public DateTime? LastUpdated { get; set; }
    }
}
