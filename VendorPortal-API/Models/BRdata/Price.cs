﻿using System;
using System.Collections.Generic;

namespace VendorPortal_API.Models.BRdata
{
    public partial class Price
    {
        public string Store { get; set; }
        public string Upc { get; set; }
        public string Vendor { get; set; }
        public short? BasePrcQty { get; set; }
        public decimal? BasePrice { get; set; }
        public short? TprprcQty { get; set; }
        public decimal? Tprprice { get; set; }
        public DateTime? TprstartDate { get; set; }
        public DateTime? TprendDate { get; set; }
        public byte? Tprpriority { get; set; }
        public int? TprpromoId { get; set; }
        public string Tprprofile { get; set; }
        public short? AdPrcQty { get; set; }
        public decimal? AdPrice { get; set; }
        public DateTime? AdStartDate { get; set; }
        public DateTime? AdEndDate { get; set; }
        public byte? AdPriority { get; set; }
        public int? AdPromoId { get; set; }
        public string AdProfile { get; set; }
        public short? FrqPrcQty { get; set; }
        public decimal? FrqPrice { get; set; }
        public short? FrqLink { get; set; }
        public string FrqLargeLink { get; set; }
        public DateTime? FrqStartDate { get; set; }
        public DateTime? FrqEndDate { get; set; }
        public byte? FrqPriority { get; set; }
        public int? FrqPromoId { get; set; }
        public string FrqProfile { get; set; }
        public short? Frq2PrcQty { get; set; }
        public decimal? Frq2Price { get; set; }
        public short? Frq2Link { get; set; }
        public string Frq2LargeLink { get; set; }
        public DateTime? Frq2StartDate { get; set; }
        public DateTime? Frq2EndDate { get; set; }
        public byte? Frq2Priority { get; set; }
        public short? DealPrcQty { get; set; }
        public decimal? DealPrice { get; set; }
        public DateTime? DealStartDate { get; set; }
        public DateTime? DealEndDate { get; set; }
        public byte? DealPriority { get; set; }
        public int? DealPromoId { get; set; }
        public string DealProfile { get; set; }
        public byte? DealLimitQty { get; set; }
        public byte? DealLimitQty2 { get; set; }
        public DateTime? LastCompCheckDate { get; set; }
        public string VendorItem { get; set; }
        public int? BuyQtyWgt { get; set; }
        public int? LimitedQty { get; set; }
        public string CouponType { get; set; }
        public string DiscountType { get; set; }
        public decimal? MinPurchase { get; set; }
        public byte? AltPriceMethod { get; set; }
        public int? ScaleAction { get; set; }
        public decimal? ScaleTareA { get; set; }
        public decimal? ScaleTareB { get; set; }
        public DateTime? LastUpdated { get; set; }
        public string Source { get; set; }
        public DateTime? BaseEffectiveDate { get; set; }
    }
}
