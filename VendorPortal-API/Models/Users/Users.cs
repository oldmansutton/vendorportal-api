﻿using System;
using System.Collections.Generic;

namespace VendorPortal_API.Models.Users
{
    public partial class Users
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public string RoleId { get; set; }
        public string VendorId { get; set; }
        public bool Approved { get; set; }
    }
}
