using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VendorPortal_API.DTO;
using VendorPortal_API.Models.BRdata;

namespace VendorPortal_API.Mapping
{
    public interface IMapItems
    {
        ItemDTO mapToDTO(Item _item, Price _price, VendorCost _cost);
    }
    public class MapItems : IMapItems
    {
        public MapItems()
        {
        }

        public ItemDTO mapToDTO(Item _item, Price _price, VendorCost _cost)
        {
            var _itemDTO = new ItemDTO()
            {
                UPC = _item.Upc,
                Desc = _item.Description,
                Dept = _item.MajorDept,
                Size = _item.SizeAlpha,
                Code = _item.Whitem,
                Pack = _cost.Pack,
                Vendor = _item.Vendor,
                Qty = _price.BasePrcQty,
                Retail = _price.BasePrice,
                Cost = _cost.CaseCost
            };
            return _itemDTO;
        }
    }
}