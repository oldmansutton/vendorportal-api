﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VendorPortal_API.DTO;
using VendorPortal_API.Models.Users;

namespace VendorPortal_API.Mapping
{
    public interface IMapUsers
    {
        Users MapFromDTO(UsersDTO usrDTO);
        UsersDTO mapToDTO(Users usr);
        object mapToDTO<T>(IEnumerable<Users> users);
    }

    public class MapUsers : IMapUsers
    {
        public MapUsers()
        {
        }

        public Users MapFromDTO(UsersDTO usrDTO)
        {
            var usr = new Users()
            {
                FirstName = usrDTO.FirstName,
                LastName = usrDTO.LastName,
                EmailAddress = usrDTO.EmailAddress,
                VendorId = usrDTO.VendorID
            };
            return usr;
        }

        public UsersDTO mapToDTO(Users usr)
        {
            var usrDTO = new UsersDTO()
            {
                FirstName = usr.FirstName,
                LastName = usr.LastName,
                EmailAddress = usr.EmailAddress,
                VendorID = usr.VendorId
            };
            return usrDTO;
        }

        public object mapToDTO<T>(IEnumerable<Users> users)
        {
            List<UsersDTO> dtos = new List<UsersDTO>();
            foreach (Users usr in users)
            {
                dtos.Add(mapToDTO(usr));
            }
            return dtos;
        }
    }
}
